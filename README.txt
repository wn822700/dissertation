Download(Unzip)all the folders

Each folder has KNN, SVM, LG, RF, Adaboost, ANN, and DNN models for their respective data

Launch Jupyter notebooks via Anaconda navigator, run each file

At the end of each file, the results of the experiments are presented in Tabular form.

Note: After you download the folders, just to view the files any IDE like visual studio, pycharm, or spyder can be used 

